r#!/bin/bash 

##appel de la variable global mit dans .bashrc

export LFS=/lfs

##Packet à installé
apt-get install -y bison
apt-get install -y gawk
apt-get install -y texinfo


##--------------------------------------------

##Creation de variable (ici lien pour packages)

WGETLIST="http://www.linuxfromscratch.org/lfs/view/stable/wget-list"
WGETLIST_FILE="wget-list"

##---------------------------------------------

##Cration du dossier source et assignations des droits a l'utilisateur

mkdir -v $LFS/sources

chmod -v a+wt $LFS/sources /

##---------------------------------------------

##Creation du dossier tools et de son lien symbolique

mkdir -v $LFS/tools

ln -sv $LFS/tools /

##----------------------------------------------

##Telecharge les dependances .


wget --input-file=$WGETLIST_FILE --continue --directory-prefix=$LFS/sources

##----------------------------------------------------------------------

##Verification que les paquets de wget list sont dispsu

cp md5sums $LFS/sources

pushd $LFS/sources

md5sum -c md5sums 

popd


##-------------------------------------------------
#Add LFS user 
groupadd lfs
useradd -s /bin/bash -g lfs -m -k/dev/null lfs

passwd lfs
chown -vR lfs $LFS/tools
chown -vR lfs $LFS/sources
chown -vR lfs $LFS/lfs-src

##-------------------------------------------------
#New .bash_profile
cat > /home/lfs/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

cat > /home/lfs/.bashrc << "EOF"
MAKEFLAGS='-j 2'
set +h
umask 022
LFS=/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/bin:/usr/bin
export LFS LC_ALL LFS_TGT PATH MAKEFLAGS
LANG=en_US.UTF-8
EOF

cp ~/.gitconfig /home/lfs/
chown lfs /home/lfs/.gitconfig
 





